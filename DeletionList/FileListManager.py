# Geeth Tharanga (c) 2017
# Distributed under MIT License
# A simple class to manage a list of files

from pathlib import Path
from typing import List,Dict
from shutil import rmtree

def _make_detail_entry(fullpath :str,name:str,isSuccess:bool,errorMsg:str,size:int) -> Dict :
    stat = {"path":fullpath, "name" : name, "success" : isSuccess, "error": errorMsg, "size": size}
    return stat

class FileListManager(object):
    """File List Manager"""

    def __init__(self):
        self._fileList = []
        self._currentFileName = None

    def set_file_list(self, fileList : List[str]) -> None:
        """Set the file list. Discards the current list"""
        self._fileList = list(set(fileList))

    def add_entry(self,file :str) -> None:
        """Add a file path to the list"""
        self._fileList.append(file)
        self._fileList = list(set(self._fileList))

    def remove_entry(self,entry :str) -> None:
        """Removes a file/dir path from the list"""
        self._fileList.remove(entry)

    def get_file_list(self) -> List[str]:
        """Get (a copy of) internal file list"""
        return list(self._fileList)

    def load_from_file(self,filename :str) -> None:
        """Load a file list from file. Discards any current list"""
        with open(filename) as file:
            lines = list(map(str.strip,file))

        #remove comments and empty lines
        lines = {line for line in lines if not line.startswith("#")}
        self._fileList = list(lines)
        self._currentFileName = filename

    def save_to_file(self,filename : str) -> None:
        """Save file list to a file"""
        with open(filename,"w") as file:
            file.writelines("\n".join(self._fileList))
        self._currentFileName = filename

    def save_current_file(self) -> None:
        """Save the current file with changes"""
        if self._currentFileName:
            self.save_to_file(self._currentFileName)
        else:
            raise RuntimeError("This file has not been saved before")

    def _get_file_details(self,filepath : str) -> Dict:
        path = Path(filepath)
        if not path.is_file():
            stats = _make_detail_entry(filepath,path.name,False,"Not a file",0)
        else:
            size = path.lstat().st_size;
            stats = _make_detail_entry(filepath,path.name,True,None,size)
        return stats

    def _get_dir_details(self,filepath : str) -> Dict:
        path = Path(filepath)
        if not path.is_dir():
            stats = _make_detail_entry(filepath,path.name,False,"Not a Directory",0)
        else:
            totalSize = 0
            success = True
            for item in path.iterdir():
                if item.is_dir():
                    subStat = self._get_dir_details(str(item))
                    totalSize += subStat["size"]
                elif item.is_file():
                    subStat = self._get_file_details(str(item))
                    totalSize += subStat["size"]

            stats = _make_detail_entry(filepath,path.name,success,None,totalSize)
        return stats

    def get_details(self) -> List[Dict]:
        """Get detailed list of files"""
        details = []
        for entry in self._fileList:
            path = Path(entry)
            if path.is_file():
                stat = self._get_file_details(entry)
            elif path.is_dir():
                stat = self._get_dir_details(entry)
            else:
                stat = _make_detail_entry(entry,path.name,False,"Not a Directory or a file",0)

            details.append(stat)

        return details

    def remove_entry_with_content(self,filepath:str) -> None:
        """Remove file/dir entry along with content on disk"""
        path = Path(filepath)
        if not path.exists():
            raise ValueError("File or Directory not found :" + filepath)
        elif filepath not in self._fileList:
            raise ValueError("File or Directory not in delete list")
        elif path.is_dir(): #directory
            rmtree(filepath)
            self.remove_entry(filepath) 
        elif path.is_file(): #file
            path.unlink() 
            self.remove_entry(filepath)
        else:
            raise ValueError("Not a File or Directory")

