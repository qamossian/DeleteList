#!/usr/bin/env python3

# Geeth Tharanga (c) 2017
# Distributed under MIT License
# A simple tool to track unimportant files and remove them when necessary

from FileListManager import FileListManager
import sys
import pathlib
from typing import List,Dict,Tuple
import os


def get_app_version() -> str:
    """Returns the app version as a str
    """
    return "1.0"



def show_help(appName : str) -> None:
    """Displays command line parameter help
    """
    print("Deletion List")
    print("Version", get_app_version())
    print("Usage :")
    print("   ",appName, "  : interactively browse delete list and remove items")
    print("   ",appName, "<file/folder name> : add an item to deletelist")



def get_filelist_manager() -> FileListManager:
    """Get the file list manager with default file list opened
    """
    appPath = pathlib.Path(os.path.realpath(__file__))
    #print("Using file list : ",appPath)
    configFileName = appPath.with_name("filelist.txt")
    manager = FileListManager()
    if configFileName.is_file(): #load file if exist
        manager.load_from_file(str(configFileName))
    else: #if file not exist, save to create empty file
        manager.save_to_file(str(configFileName))
    return manager


def print_pathlist(fileList: List[str]) -> None:
    """ Print path list with indices in interactive mode
    """
    if len(fileList) == 0:
        print("No files added to the list")
    else:
        print("Files/Dir List:")
        for (index,path) in enumerate(fileList,start=1):
            print (index, ":",path)

def human_friendly_size(size: int) -> str:
    """Returns a string with given file size(bytes) in a human friendly way
    """
    suffixes = ["B","KB","MB"]
    for suf in suffixes:
        if size < 1024:
            return "{:.2f} {}".format(size,suf)
        else:
            size /= 1024
    return "{:.2f} {}".format(size,"GB")

def print_detailedList(fileList: List[Dict]) -> None:
    """Print path list with additional details"""
    if len(fileList) == 0:
        print("No files added to the list")
    else:
        print("Files/Dir List:")
        for (index,item) in enumerate(fileList,start=1):
            print (index, ":",item["path"])
            if item["success"]:
                print("\t","Size :",human_friendly_size(item["size"]))
            else:
                print("\t","Error :",item["error"])

def print_choices() -> None:
    """Print default choices at the interactive mode
    """
    print("")
    print("d <index> [index2]... to delete file. (eg: 'd 3', 'd 3 4')")
    print("r <index> [index2]... to remove entry. (eg: 'r 2', 'r 2 4 5')")
    print("c <size in MB> delete entries to free given space. (eg: 'c 200' will free atleast 200 mb)")
    print("l for to display path list again")
    print("i retrieves size info")
    print("x or q to exit") 

def input_choice(allowedChoices : List[str]) -> Tuple[str,List[str]]:
    """Inputs a choice from the user.
    Allowed choices are list of characters which stands for the commands
    Returns a tuple of choice and parameter list
    """
    while True:
        inp = str(input(">>")).strip()
        if inp:
            cmd = inp[0]
            if cmd not in allowedChoices:
                print("Invalid choice")
            else:
                params = inp[1:].strip().replace(","," ").replace("\t"," ").split(" ")
                params = list(filter(bool,params))
                return (cmd,params)

def remove_entry(paramIndices : List[str], manager : FileListManager) -> None:
    """Remove given entry(ies) in file list without deleting on disk
    """
    intParams = None
    try:
        intParams = list(map(int,paramIndices))
    except ValueError:
        print("Invalid index. Indices must be parameters")

    fileList = manager.get_file_list()
    indexrange = range(1, len(fileList) + 1)
    invalidIndices = [str(i) for i in intParams if not i in indexrange]
    if invalidIndices:
        print("Invalid indices found: ", ", ".join(invalidIndices))
        return None
    else:
        for idx in intParams:
            path = fileList[idx-1]
            manager.remove_entry(path)
        manager.save_current_file()

def delete_items(paramIndices : List[str], manager : FileListManager) -> None:
    """Delete give paths from file list and their content on disk
    """
    intParams = None
    try:
        intParams = list(map(int,paramIndices))
    except ValueError:
        print("Invalid index. Indices must be parameters")

    fileList = manager.get_file_list()
    indexrange = range(1, len(fileList) + 1)
    invalidIndices = [str(i) for i in intParams if not i in indexrange]
    if invalidIndices:
        print("Invalid indices found: ", ",".join(invalidIndices))
        return None
    else:
        pathList = [fileList[i-1] for i in intParams]
        for path in pathList:
            print("Deleting ",path,"...")
            manager.remove_entry_with_content(path)
        manager.save_current_file()

def clear_space(sizeInBytes: int, manager : FileListManager) -> None:
    """Delete paths and their content to free at least a given amount of space
    """
    detailedList = manager.get_details()
    sizeCleared = 0
    while sizeCleared < sizeInBytes and len(detailedList) > 0:
        item = detailedList.pop(0)
        sizeCleared += item["size"]
        print("Deleting ",item["path"],"...")
        manager.remove_entry_with_content(item["path"])
    print(human_friendly_size(sizeCleared), "of space cleared")

def open_interactive() -> None:
    """Begins the interactive mode
    """
    manager = get_filelist_manager()
    print("--- Delete list",get_app_version(),"---")
    
    fileList = manager.get_file_list()
    print_pathlist(fileList)
    print_choices()

    exitRequested = False
    while not exitRequested:
        (choice,params) = input_choice(["d","r","c","x","q","i","l"])
        
        if choice in ["q", "x"]:
            exitRequested = True
        elif choice == "i": #size info
            detailedList = manager.get_details()
            print_detailedList(detailedList)
        elif choice == "l":
            fileList = manager.get_file_list()
            print_pathlist(fileList)
        elif choice == "r":
            remove_entry(params,manager)
        elif choice == "d":
            delete_items(params,manager)
        elif choice == "c":
            if len(params) != 1:
                print("Clear disk space command requires exactly 1 argument")
            else:
                try:
                    sizeMb = int(params[0])
                    clear_space(sizeMb*1024*1024,manager)
                except ValueError:
                    print("Invalid size")

        


def add_files(file_paths: List[str]):
    """Add paths to current file list and save
    """
    manager = get_filelist_manager()
    for path in file_paths:
        manager.add_entry(path)
    manager.save_current_file()


def main(argv: List[str]) -> None:
    """Main entry point of the app
    """
    appPath = pathlib.Path(argv[0])
    
    if(len(argv) == 1): #no params
        open_interactive()
    else: #has file names cmd parameters
        add_files(argv[1:])


if __name__ == "__main__":
    main(sys.argv)
