Deletion List
================
A simple tool to keep track of unimportant files and helps you 
to gain disk space when needed by deleting them.

Adding unimportant files/dirs to list
---------------------------------------
Use `DeletionList.py <path 1> [path 2]...` to add the paths to the list.  
You can also add it to "Send To" context menu in file explorer 
on windows (see instructions below).

Unmarking items and deleting files
-----------------------------------
Use `DeletionList.py` without any command line parameters to interactively
browse the list and delete any files.

System Requirements
---------------------
* Requires Python 3.5  
* Should work for any environment with python 3.5  
* No 3rd party libraries are required

License
-----------
Distributed under MIT License. View LICENSE file for more details

Adding entry to *Send To* menu in windows
-----------------------------------------
Go to "Send To" Folder by navigating to `shell:sendto` in windows explorer 
or Run command (Window + R).  
Create a shortcut for DeletionList.py with target `pyw <full path to DeletionList.py>`